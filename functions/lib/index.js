"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const firebaseFirestore = require("firebase-admin");
const distance_matrix = require("google-distance-matrix");
class Values {
}
Values.nearestNodeLength = 0; //by default
class Waste {
    constructor(SourceType, SourceLat, SourceLon, SourceId, SourceStatus) {
        this.SourceType = SourceType;
        this.SourceLat = SourceLat;
        this.SourceLon = SourceLon;
        this.SourceId = SourceId;
        this.SourceStatus = SourceStatus;
        this.sourceId = SourceId;
        this.sourceLat = SourceLat;
        this.sourceLon = SourceLon;
        this.sourceId = SourceId;
        this.sourceStatus = SourceStatus;
    }
}
class WasteNode {
    constructor(Distance, Duration, Rank, Visited) {
        this.Distance = Distance;
        this.Duration = Duration;
        this.Rank = Rank;
        this.Visited = Visited;
        this.visited = Visited;
        this.rank = Rank;
        this.distance = Distance;
        this.duration = Duration;
    }
}
// import { error } from 'util';
// // Start writing Firebase Functions
// // https://firebase.google.com/functions/write-firebase-functions
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
// import { DeltaSnapshot } from 'firebase-functions/lib/providers/database';
// import { event } from 'firebase-functions/lib/providers/analytics';
// const functions = require('firebase-functions')
// const firebaseFirestore = require('firebase-admin')
// const distance_matrix=require('google-distance-matrix')
firebaseFirestore.initializeApp(functions.config().firebase);
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
// exports.firestore=functions.firestore.document('wastes/{wildCard}').onWrite(event=>{
//     const pickerDoc=event.data.data()
//     if(pickerDoc.done){
//         return
//     }
//     console.log("New driver request from id "+pickerDoc)
//     pickerDoc.done=true
//     return event.data.ref.set(pickerDoc)
// });
// exports.nearbyService=functions.database.ref("kawadi/services/nearbywaste/{wildCard}").onWrite(event=>{
//     var newNode=event.data.val()//get the whole object
//     if(newNode.already){
//         return
//     }
//     console.log("updating that new object")
//     newNode.already=true
//     newNode.title="new title"
//     newNode.body="new body"
//     return event.data.ref.set(newNode)//returns promises
// });
exports.nearbyWastes = functions.firestore.document("pickers/{wildCard}/nearbyplaces/nearbyplaceDoc").onWrite(event => {
    const nearByplaceDoc = event.data.data();
    if (nearByplaceDoc.done) {
        return;
    }
    console.log("Request came from a truck driver " + nearByplaceDoc.truckId);
    nearByplaceDoc.done = true;
    const truckId = nearByplaceDoc.truckid; //get the truckid
    return event.data.ref.set(nearByplaceDoc).then(res => {
        return firebaseFirestore.firestore().collection('wastes').get().then(snapshot => {
            var recommendedWastes = []; //arrays of dynamic size
            // var origins=[]
            var destinations = [];
            var destinations2 = [];
            let wasteStack = [];
            let wasteStackRank = []; //container of objects of type waste
            let nearbyWasteStack = []; //top nearest among many
            let eachWaste;
            snapshot.forEach((eachDoc) => {
                // console.log("wastes id "+eachDoc.id)
                // console.log("wastes data "+eachDoc.data())
                recommendedWastes.push(eachDoc.data());
                // origins.push(eachDoc.data().sourceLat+","+eachDoc.data().sourceLon)
                destinations.push(eachDoc.data().sourceLat + "," + eachDoc.data().sourceLon);
                destinations2.push(eachDoc.data().sourceLat + "," + eachDoc.data().sourceLon);
                console.log("each destinations" + destinations.pop());
            });
            var data = {
                foo: recommendedWastes
            };
            //google distance matrix
            return new Promise((resolve, reject) => {
                distance_matrix.key('AIzaSyBBX6pCmyvDIFmrD3FAh7WzDpls0kfOTZg');
                var origins = ['27.670847,85.314860'];
                // var destinations=['27.698840,85.324860']
                distance_matrix.matrix(origins, destinations2, function (err, distances) {
                    if (distances['status'] == "OK") {
                        resolve(distances);
                    }
                    else {
                        reject(err);
                    }
                });
            }).then(distances => {
                /*----------------begin nearby implementation------------------------------------*/
                console.log("json response " + JSON.stringify(distances));
                var rowsObject = distances['rows'];
                rowsObject.forEach(eachROws => {
                    var elements = eachROws['elements'];
                    elements.forEach(eachElements => {
                        if (eachElements.status == "OK") {
                            //    console.log("distance text "+eachElements.distance.text) 
                            //    console.log("distance value "+eachElements.distance.value) 
                            //    console.log("distance text "+eachElements.duration.text) 
                            //    console.log("distance text "+eachElements.duration.value)
                            eachWaste = new WasteNode(eachElements.distance.value, eachElements.duration.value, 0, "N"); //create new waste everytime
                            wasteStack.push(eachWaste);
                            // wasteStackRank.push(eachWaste.rank)
                        }
                    });
                });
                /*perform sorting i.e top 5 nearest node*/
                wasteStack.sort(function (waste1, waste2) { return waste1.distance - waste2.distance; });
                //sorting the arrays in ascending order #for descending order b-a
                // if(wasteStackRank.length<=5){
                //     Values.nearestNodeLength=wasteStackRank.length
                //     chainAlgorithm(wasteStack)
                // } 
                // Values.nearestNodeLength=wasteStackRank.length
                console.log("size is >5");
                chainAlgorithm(wasteStack);
            }
            /*----------------------------------------end of implement-----------------------*/
            ).then(update => {
                //then update the firebase node
                // return firebaseFirestore.firestore().doc("pickers/hello/nearbyplaces/recommendedWastes").set({recommendedWastes}).then(recom=>{
                //     console.log("recommendations object send ")
                // })
            })
                .catch(err => {
                //notify unsuccess to the client that requested
                console.log("error is " + err);
            });
        });
    });
});
function chainAlgorithm(wasteStack) {
    //check if no nearby wastes
    console.log("ascending order $size " + wasteStack.length);
    wasteStack.forEach(element => {
        console.log(" distance: " + element.distance);
    });
}
//# sourceMappingURL=index.js.map