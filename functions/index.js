import { error } from 'util';

// import { DeltaSnapshot } from 'firebase-functions/lib/providers/database';

// import { event } from 'firebase-functions/lib/providers/analytics';



const functions = require('firebase-functions')
const firebaseFirestore = require('firebase-admin')
const distance_matrix=require('google-distance-matrix')

firebaseFirestore.initializeApp(functions.config().firebase)

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

// exports.firestore=functions.firestore.document('wastes/{wildCard}').onWrite(event=>{

//     const pickerDoc=event.data.data()
//     if(pickerDoc.done){
//         return
//     }
//     console.log("New driver request from id "+pickerDoc)
//     pickerDoc.done=true
//     return event.data.ref.set(pickerDoc)
    
// });

// exports.nearbyService=functions.database.ref("kawadi/services/nearbywaste/{wildCard}").onWrite(event=>{
//     var newNode=event.data.val()//get the whole object
//     if(newNode.already){
//         return
//     }
//     console.log("updating that new object")
//     newNode.already=true
//     newNode.title="new title"
//     newNode.body="new body"
//     return event.data.ref.set(newNode)//returns promises
    

// });

exports.nearbyWastes=functions.firestore.document("pickers/{wildCard}/nearbyplaces/nearbyplaceDoc").onWrite(event=>{
    const nearByplaceDoc=event.data.data()
    if(nearByplaceDoc.done){
        return
    }
    console.log("Request came from a truck driver "+nearByplaceDoc.truckId)
    nearByplaceDoc.done=true

    const truckId=nearByplaceDoc.truckid//get the truckid

    return event.data.ref.set(nearByplaceDoc).then(res=>{
        return   firebaseFirestore.firestore().collection('wastes').get().then(snapshot=>{
            var recommendedWastes=[]//arrays of dynamic size
            snapshot.forEach((eachDoc)=>{
                console.log("wastes id "+eachDoc.id)
                console.log("wastes data "+eachDoc.data())
                recommendedWastes.push(eachDoc.data())
                

            })            
            var data={
                foo:recommendedWastes
            };

            //google distance matrix

            return new Promise((resolve,reject)=>{

                distance_matrix.key('AIzaSyBBX6pCmyvDIFmrD3FAh7WzDpls0kfOTZg')
                var origins=['27.688840,85.314860']
                var destinations=['27.698840,85.324860']
                distance_matrix.matrix(origins,destinations,function(err,distances){
                    if(distances['status']=="OK"){
                        resolve(distances)
                    }else{
                        reject(err)
                  }
                })

            }).then(distances=>{
                /*----------------begin nearby implement------------------------------------*/
                var rowsObject=distances['rows']
                rowsObject.forEach(eachROws=>{
                    var elements=eachROws['elements']
                    elements.forEach(eachElements=>{
                        if(eachElements.status=="OK"){
                       console.log("distance text "+eachElements.distance.text) 
                       console.log("distance value "+eachElements.distance.value) 
                       console.log("distance text "+eachElements.duration.text) 
                       console.log("distance text "+eachElements.duration.value)
                        }
                       
                   })
                })
                /*----------------------------------------end of implement-----------------------*/
                //then update the firebase node
                return firebaseFirestore.firestore().doc("pickers/hello/nearbyplaces/recommendedWastes").set({recommendedWastes}).then(recom=>{
                    console.log("recommendations object send "+ recom)
                })

            })
            .then(()=>{
                console.log("another then statement")
            })
            .catch(error=>{
                //notify unsuccess to the client that requested
                console.log("error is "+error)
            })
    
        })
    })


})







